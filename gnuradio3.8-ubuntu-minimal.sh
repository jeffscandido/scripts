#! /bin/bash

sudo apt update && sudo apt install -y autoconf automake bison clang-9 doxygen flex git libboost-all-dev libcodec2-0.7 libcodec2-dev libdpdk-dev libevent-dev libfftw3-bin libfftw3-dev libgmp-dev libgmp3-dev libgmpxx4ldbl libgsl-dev libgsl23 libjack-dev liblog4cpp5-dev liblog4cpp5v5 libncurses5 libncurses5-dev liborc-0.4-0 liborc-0.4-dev libpjmedia-codec2 libportaudio2 libpthread-stubs0-dev libqwtmathml-qt5-6 libqwtmathml-qt5-dev libsdl1.2-dev libssl1.0-dev libssl1.0.0 libtool libudev-dev libusb-1.0-0 libusb-1.0-0-dev libusb-dev libzmq3-dev libzmq5 libzmqpp-dev libzmqpp4 make ncurses-bin pkg-config portaudio19-dev python-backports.ssl-match-hostname python-gevent python-ipaddress python-six python-thrift python3-aptdaemon.gtk3widgets python3-cairo python3-cairo-dev python3-click python3-click-plugins python3-gevent python3-gi-cairo python3-mako python3-numpy python3-pyaudio python3-pyqt5 python3-setuptools python3-six python3-sphinx python3-thriftpy snapd swig yasm

sudo snap install cmake --classic

echo 'export PATH="/snap/bin:$PATH"' >> $HOME/.bashrc
source $HOME/.bashrc

# sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 100 --slave /usr/bin/g++ g++ /usr/bin/g++-8

cd $HOME

# INSTALAÇÃO DA BIBLIOTECA MPIR: Multiple Precision Integers and Rationals
wget http://mpir.org/mpir-3.0.0.tar.bz2
tar xjf mpir-3.0.0.tar.bz2
cd mpir-3.0.0
./configure --enable-cxx
make && make check && sudo make install && sudo ldconfig

cd $HOME

# INSTALAÇÃO Thrifth Apache
# https://wiki.gnuradio.org/index.php/ControlPort
git clone https://github.com/apache/thrift.git
cd thrift
git checkout v0.13.0
./bootstrap.sh
./configure --prefix=$prefix PY_PREFIX=$prefix --with-cpp --with-python CXXFLAGS="-DNDEBUG"
make && make check && sudo make install && sudo ldconfig && thrift -version

cd $HOME
# INSTALAÇÃO DO GNURADIO 3.8.0
git clone --recursive https://github.com/gnuradio/gnuradio.git
cd gnuradio
git checkout maint-3.8
cd volk
git checkout v2.2.0
cd ..
mkdir build
sed -i 's/\/usr\/include/\/usr\/include\n  \/include/g' $HOME/gnuradio/cmake/Modules/FindTHRIFT.cmake
sed -i 's/\/usr\/lib/\/usr\/lib\n  \/lib/g' $HOME/gnuradio/cmake/Modules/FindTHRIFT.cmake

cd build

cmake -DCMAKE_BUILD_TYPE=Release -DPYTHON_EXECUTABLE=/usr/bin/python3 $HOME/gnuradio

make -j2 && make test && sudo make install && sudo ldconfig

echo 'export LD_LIBRARY_PATH=/usr/local/lib' >> $HOME/.bashrc

gnuradio-config-info --version
gnuradio-config-info --prefix
gnuradio-config-info --enabled-components