#! /bin/bash

temp_root_dir=$HOME
cd ${temp_root_dir}

sudo apt-get update
echo -e "\n\nSYTEM UPDATED\n\n"
sudo apt-get upgrade
echo -e "\n\nSYTEM UPGRADED\n\n"

sudo apt-get install -y autoconf doxygen g++-8 gcc-8 git libboost-all-dev libdpdk-dev libncurses5 libncurses5-dbg libncurses5-dev libudev-dev libusb-1.0-0 libusb-1.0-0-dev libusb-dev ncurses-bin python3-numpy python3-setuptools

sudo snap install cmake --classic

git clone https://github.com/EttusResearch/liberio.git
cd ${temp_root_dir}/liberio
autoreconf -i
./configure --prefix=/usr
make && sudo make install
echo -e "\n\nDEPENDENCIES INSTALLED\n\n"

cd ${temp_root_dir}
git clone https://github.com/EttusResearch/uhd
cd ${temp_root_dir}/uhd
git checkout v3.15.0.0
cd ${temp_root_dir}/uhd/host
mkdir build
cd ${temp_root_dir}/uhd/host/build
cmake ../
proc_processor=$(grep 'processor' /proc/cpuinfo | sort -u | wc -l)

echo -e "\n\nBUILDING UHD\n\n"
make -j${proc_processor}
echo -e "\n\nUHD BUILDED"

echo -e "\n\nTESTING UHD BUILDING\n\n"
make test
echo -e "\n\nUHD BUILDING TESTED"
echo -e "\n\nINSTALLING UHD ETTUS RESEARCH\n\n"
sudo make install
echo -e "\n\nUHD ETTUS RESEARCH INSTALLED"
sudo ldconfig

echo -e "\n\nEXPORTING LD_LIBRARY_PATH\n\n"
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib' >> $HOME/.bashrc
sudo uhd_images_downloader